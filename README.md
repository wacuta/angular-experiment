# AngularTest

> [Angular CLI](https://github.com/angular/angular-cli) version 15.0.4.

## Development server

Create `environements\environement.ts` with `EnvironmentInterface` and run `ng serve` for a dev server. Navigate to [`http://localhost:4200/`](http://localhost:4200/). 

## CLI

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
