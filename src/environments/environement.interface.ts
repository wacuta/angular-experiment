export interface EnvironmentInterface {
  dynamodb: {
    accessKeyId : string,
    secretAccessKey : string,
    region: string,
    tableName: string
  }
}
