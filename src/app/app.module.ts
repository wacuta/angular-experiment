import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AwsDynamodbComponent } from './aws-dynamodb/aws-dynamodb.component';
import { PreferenceResolver } from './aws-dynamodb/resolver/preference.resolver';
import { MapLeafletComponent } from './map-leaflet/map-leaflet.component';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { LeafletDrawModule } from '@asymmetrik/ngx-leaflet-draw';
import { MapGoogleComponent } from './map-google/map-google.component';
import { GoogleMapsModule } from '@angular/google-maps';
import { HttpClientModule } from '@angular/common/http';
import { ComponentsModule } from './components/components.module';
import { SandboxComponent } from './sandbox/sandbox.component';

@NgModule({
  declarations: [
    AppComponent,
    SandboxComponent,
    AwsDynamodbComponent,
    MapLeafletComponent,
    MapGoogleComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    LeafletModule,
    LeafletDrawModule,
    GoogleMapsModule,
    HttpClientModule,
    ComponentsModule,
    FormsModule
  ],
  providers: [
    PreferenceResolver
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
