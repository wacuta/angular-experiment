import { CommonModule } from '@angular/common';
import { Component, ElementRef, Input } from '@angular/core';


@Component({
  standalone: true,
  imports: [
    CommonModule
  ],
  selector: 'app-fab-action',
  templateUrl: './fab-action.component.html',
  styleUrls: ['./fab-action.component.scss']
})
export class FabActionComponent {

  constructor(public elementRef: ElementRef) {}

  @Input() label!: string;
  @Input() icon!: string;

}
