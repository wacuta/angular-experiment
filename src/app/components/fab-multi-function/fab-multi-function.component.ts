import { CommonModule } from '@angular/common';
import { Component, ContentChildren, ElementRef, EventEmitter, Input, Output, QueryList, Renderer2, ViewEncapsulation } from '@angular/core';
import { FabActionComponent } from './fab-action/fab-action.component';
import { FabMultiFunctionTypeEnum } from './fab-multi-function-type.enum';


@Component({
  standalone: true,
  encapsulation: ViewEncapsulation.None,
  imports: [
    CommonModule
  ],
  selector: 'app-fab-multi-function',
  templateUrl: './fab-multi-function.component.html',
  styleUrls: ['./fab-multi-function.component.scss']
})
export class FabMultiFunctionComponent {

  @Input() mode: FabMultiFunctionTypeEnum = FabMultiFunctionTypeEnum.HOVER;

  @Input() mainIcon!: string;

  @Output() mainClick = new EventEmitter();

  @ContentChildren(FabActionComponent) actions!: QueryList<FabActionComponent>;

  fab!: any;

  private isOpen: boolean = false;
  private hoverTimeout: any;

  constructor(
    private elementRef: ElementRef,
    private renderer: Renderer2
  ) { }

  ngOnInit(): void {
    this.fab = this.elementRef.nativeElement.querySelector('.fab');
    this.renderer.listen('window', 'click', (e:Event)=>{
        this.onClick(this.fab.contains(e.target))
    });

    this.fab.querySelector('app-fab-action:last-child').addEventListener('animationend', () => {
      if(!this.isOpen) {
        this.fab.querySelector('.fab_actions').style['height'] = '0px';
        this.fab.querySelector('.fab_actions').style['padding-bottom'] = '0';
      }
    });
  }

  onClick(show: boolean = true): void {
    if(this.mode !== FabMultiFunctionTypeEnum.CLICK) return;

    if(show) {
      this.showActions();
    } else {
      this.hideActions();
    }
  }

  onMouseEnter(): void {
    if(this.mode !== FabMultiFunctionTypeEnum.HOVER) return;
    this.hoverTimeout = setTimeout(() => this.showActions(), 200);
  }

  onMouseLeave(): void {
    if(this.mode !== FabMultiFunctionTypeEnum.HOVER) return;
    clearTimeout(this.hoverTimeout);
    this.hideActions();
  }

  private showActions(): void {
    if(!this.isOpen) {
      this.fab.classList.remove('close');
      this.fab.classList.add('open');
      this.fab.querySelector('.fab_actions').style['height'] = 'auto';
      this.fab.querySelector('.fab_actions').style['padding-bottom'] = '10px';
      this.actions.forEach((action: FabActionComponent, index: number) => {
        this.showElement(action.elementRef.nativeElement, index)
      });
      this.isOpen = true;
    }
  }

  private hideActions(): void {
    if(this.isOpen) {
      this.fab.classList.remove('open');
      this.fab.classList.add('close');
      this.actions.forEach((action: FabActionComponent, index: number) => {
        this.hideElement(action.elementRef.nativeElement, index)
      });
      this.isOpen = false;
    }
  }

  /**
   * Need to hide element step by step, from top to bottom
   */
  private hideElement(element: any, index: number): void {
    element.style['animation-delay'] = 0.1 + index * 0.08 + 's'
  }

  /**
   * Need to show element step by step, from bottom to top
   */
  private showElement(element: any, index: number): void {
    element.style['animation-delay'] = (this.actions.length * 0.1) - index * 0.05 + 's'
  }
}
