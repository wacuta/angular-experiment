import { NgModule } from '@angular/core';
import { FabMultiFunctionComponent } from './fab-multi-function/fab-multi-function.component';
import { FabActionComponent } from './fab-multi-function/fab-action/fab-action.component';

@NgModule({
  imports: [
    FabMultiFunctionComponent,
    FabActionComponent
  ],
  exports: [
    FabMultiFunctionComponent,
    FabActionComponent
  ],
  declarations: [],
  providers: [ ],
})
export class ComponentsModule {
}
