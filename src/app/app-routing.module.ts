import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AwsDynamodbComponent } from './aws-dynamodb/aws-dynamodb.component';
import { PreferenceResolver } from './aws-dynamodb/resolver/preference.resolver';
import { MapLeafletComponent } from './map-leaflet/map-leaflet.component';
import { MapGoogleComponent } from './map-google/map-google.component';
import { SandboxComponent } from './sandbox/sandbox.component';

const routes: Routes = [
  {
    path: '',
    component: SandboxComponent
  },
  {
    path: 'aws-dynamodb',
    component: AwsDynamodbComponent,
    resolve: {
      preference: PreferenceResolver
    }
  },
  {
    path: 'map-leaflet',
    component: MapLeafletComponent
  },
  {
    path: 'map-google',
    component: MapGoogleComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
