import { Component } from '@angular/core';
import { UntypedFormBuilder } from '@angular/forms';
import { ActivatedRoute, Data } from '@angular/router';
import { AwsService } from './service/aws-service';

@Component({
  selector: 'app-aws-dynamodb',
  templateUrl: './aws-dynamodb.component.html',
  styleUrls: ['./aws-dynamodb.component.scss']
})
export class AwsDynamodbComponent {

  userId : string = "642";

  preference: string = 'bus-stop'

  tables: string[] = [];
  fields: string[] = [];
  columns: string[] = [];


  constructor(
    protected service: AwsService,
    protected fb: UntypedFormBuilder,
    protected route: ActivatedRoute
  ) {

  }

  ngOnInit()  {
    this.route.data.subscribe((data: Data) => {
      console.log("resolver return", data["preference"])
      this.columns = data["preference"]?.["poi.column"];
    });
  }

  createTable() {
    this.service.createTable();
  }

  describeTable() {
    this.service.describeTable();
  }

  deleteTable() {
    this.service.deleteTable();
  }


  show() {
    this.service.getAll(this.userId, 'preference')
      .then(response => console.log(response))
  }

  assignField() {
    this.service.put(this.userId, 'preference', 'bus-stop.filters', `"[{\"enabled\":true,\"draggable\":true,\"name\":\"Nom\",\"key\":\"name\"},{\"enabled\":true,\"draggable\":true,\"name\":\"Code\",\"key\":\"code\"},{\"enabled\":true,\"draggable\":true,\"name\":\"Propriété\",\"key\":\"125_property\"},{\"enabled\":false,\"draggable\":true,\"name\":\"Description\",\"key\":\"description\"},{\"enabled\":true,\"draggable\":true,\"name\":\"Type d'arrêt\",\"key\":\"temporary\"},{\"enabled\":false,\"draggable\":true,\"name\":\"Type de lignes\",\"key\":\"line_types\"},{\"enabled\":false,\"draggable\":true,\"name\":\"Lignes\",\"key\":\"lines\"},{\"enabled\":false,\"draggable\":true,\"name\":\"Direction\",\"key\":\"direction\"},{\"enabled\":false,\"draggable\":true,\"name\":\"Nombre de lignes passantes\",\"key\":\"number_of_line_directions\"},{\"enabled\":false,\"draggable\":true,\"name\":\"Secteurs de pose\",\"key\":\"sector_pose\"},{\"enabled\":false,\"draggable\":true,\"name\":\"Secteurs de maintenance\",\"key\":\"sector_maintenance\"},{\"enabled\":false,\"draggable\":true,\"name\":\"Type de Mobiliers\",\"key\":\"furniture_types\"},{\"enabled\":true,\"draggable\":true,\"name\":\"je suis un cp mep 52\",\"key\":\"mep-52\"},{\"enabled\":false,\"draggable\":true,\"name\":\"Mobiliers\",\"key\":\"furnitures\"},{\"enabled\":false,\"draggable\":true,\"name\":\"Villes\",\"key\":\"cities\"},{\"enabled\":false,\"draggable\":true,\"name\":\"Actif\",\"key\":\"enabled\"},{\"enabled\":false,\"draggable\":true,\"name\":\"Accessible\",\"key\":\"accessible\"},{\"enabled\":false,\"draggable\":true,\"name\":\"Aperçu\",\"key\":\"no_medias\"},{\"enabled\":false,\"draggable\":true,\"name\":\"Maintenance\",\"key\":\"maintenance\"},{\"enabled\":false,\"draggable\":true,\"name\":\"Champ perso pls lignes PA\",\"key\":\"CP_ML_PA\"},{\"enabled\":false,\"draggable\":true,\"name\":\"Champ perso booléen PA\",\"key\":\"CP_BOOL_PA\"},{\"enabled\":false,\"draggable\":true,\"name\":\"Champ perso numérique pour point d'arrêt\",\"key\":\"CP_PA_NUM\"},{\"enabled\":false,\"draggable\":true,\"name\":\"mep\",\"key\":\"mep\"},{\"enabled\":false,\"draggable\":true,\"name\":\"gfgdfhj\",\"key\":\"125487\"},{\"enabled\":false,\"draggable\":true,\"name\":\"Présence d'une poubelle\",\"key\":\"poubelle\"},{\"enabled\":false,\"draggable\":true,\"name\":\"coucou custom field\",\"key\":\"coucou_custom_field\"},{\"enabled\":false,\"draggable\":true,\"name\":\"custom\",\"key\":\"custom2\"},{\"enabled\":false,\"draggable\":true,\"name\":\"Modèle de mobilier\",\"key\":\"125_variety\"},{\"enabled\":false,\"draggable\":true,\"name\":\"Caisson d'information\",\"key\":\"125_publicity_box\"},{\"enabled\":false,\"draggable\":true,\"name\":\"Eclairage\",\"key\":\"125_solar_panel\"}]"`)
      .then(response => console.log(response))
  }

  deleteField() {
    this.service.delete(this.userId, 'preference', 'bus-stop.filters')
      .then(response => console.log(response))
  }


}
