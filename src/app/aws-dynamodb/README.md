# AWS DynamoDB

Le module va charger les données en base avant de les stocker en localStorage. On peut en ajouter et en supprimer, ça va mettre pousser la modif en base et mettre à jour les localStorage en consequence.

[DynamoDB](https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/DynamoDB.html)
[DocumentClient](https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/DynamoDB/DocumentClient.html)

```
The primary key attribute values that define the items and the attributes associated with the items.

S — (String)
An attribute of type String. For example:

"S": "Hello"

N — (String)
An attribute of type Number. For example:

"N": "123.45"

Numbers are sent across the network to DynamoDB as strings, to maximize compatibility across languages and libraries. However, DynamoDB treats them as number type attributes for mathematical operations.

B — (Buffer, Typed Array, Blob, String)
An attribute of type Binary. For example:

"B": "dGhpcyB0ZXh0IGlzIGJhc2U2NC1lbmNvZGVk"

SS — (Array<String>)
An attribute of type String Set. For example:

"SS": ["Giraffe", "Hippo" ,"Zebra"]

NS — (Array<String>)
An attribute of type Number Set. For example:

"NS": ["42.2", "-19", "7.5", "3.14"]

Numbers are sent across the network to DynamoDB as strings, to maximize compatibility across languages and libraries. However, DynamoDB treats them as number type attributes for mathematical operations.

BS — (Array<Buffer, Typed Array, Blob, String>)
An attribute of type Binary Set. For example:

"BS": ["U3Vubnk=", "UmFpbnk=", "U25vd3k="]

M — (map<map>)
An attribute of type Map. For example:

"M": {"Name": {"S": "Joe"}, "Age": {"N": "35"}}

L — (Array<map>)
An attribute of type List. For example:

"L": [ {"S": "Cookies"} , {"S": "Coffee"}, {"N": "3.14159"}]

NULL — (Boolean)
An attribute of type Null. For example:

"NULL": true

BOOL — (Boolean)
An attribute of type Boolean. For example:

"BOOL": true
```