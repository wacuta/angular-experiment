import { Injectable } from '@angular/core';
import * as AWS from 'aws-sdk';
import * as DynamoDB from 'aws-sdk/clients/dynamodb';
import { DocumentClient } from 'aws-sdk/clients/dynamodb';
import { environment } from 'src/environments/environement';

@Injectable({
  providedIn: 'root'
})
export class AwsService {

    readonly TABLE_NAME: string = environment.dynamodb.tableName;

    private dynamodb: DynamoDB;
    private docClient: DocumentClient;

    constructor() {
        AWS.config.update({
            accessKeyId : environment.dynamodb.accessKeyId,
            secretAccessKey : environment.dynamodb.secretAccessKey,
            region: environment.dynamodb.region
        });

        this.dynamodb = new AWS.DynamoDB();
        this.docClient = new AWS.DynamoDB.DocumentClient();
    }

    /**
     * lister les tables
     */
    listerTable(): void
    {
        const params = {
            Limit: 10
        };
        this.dynamodb.listTables(params, (err, data) => {
            if (err) {
                console.error(err.code);
            } else {
                console.log("listerTable", data)
            }
        });
    }

    /**
     * Create Table
     */
    createTable(): void
    {
        var params = {
        TableName: this.TABLE_NAME,
        AttributeDefinitions: [
            {
                AttributeName: 'USER_ID',
                AttributeType: 'S'
            },
            {
                AttributeName: 'TYPE',
                AttributeType: 'S'
            }
        ],
        KeySchema: [
            {
                AttributeName: 'USER_ID',
                KeyType: 'HASH'
            },
            {
                AttributeName: 'TYPE',
                KeyType: 'RANGE'
            }
        ],
        ProvisionedThroughput: {
            ReadCapacityUnits: 1,
            WriteCapacityUnits: 1
        },
        StreamSpecification: {
            StreamEnabled: false
        }
        };

        // Call DynamoDB to create the table
        this.dynamodb.createTable(params, function(err, data) {
          if (err) console.error(err);
          else     console.log("Table Created", data);
        });
    }

    /**
     * Détails de la table
     */
    describeTable(): void
    {
        var params = {
            TableName: this.TABLE_NAME
        };
        this.dynamodb.describeTable(params, function(err, data) {
            if (err) console.error(err, err.stack);
            else     console.log("describeTable", data);
        });
    }

    /**
     * Suppression de la table
     */
    deleteTable(): void
    {
        var params = {
            TableName: this.TABLE_NAME
        };
        this.dynamodb.deleteTable(params, function(err, data) {
            if (err) console.error(err, err.stack); // an error occurred
            else     console.log("deleteTable", data);
        });
    }

    /**
     * Récupère un Item
     *
     * @param userId l'id du User
     * @param type le type d'information que l'on veut récupérer
     * @param key la clé dans le document
     * @returns Observale<{key : []}>
     */
    get(userId: string, type: string, key: string|string[]): Promise<any>
    {
        if (typeof key == "string") {
            key = <string[]>[key];
        }

        var params = {
            TableName: this.TABLE_NAME,
            Key: {
                "USER_ID": userId,
                "TYPE": type
            },
            AttributesToGet: [...key]
        };

        return this.docClient.get(params).promise()
            .then((data: DynamoDB.GetItemOutput) => {
                return data.Item
            })
    }

    /**
     * Récupère un Item
     *
     * @param userId l'id du User
     * @param type le type d'information que l'on veut récupérer
     * @returns Observale<{key : []}>
     */
    getAll(userId: string, type: string): Promise<any>
    {
        var params = {
            TableName: this.TABLE_NAME,
            Key: {
                "USER_ID": userId,
                "TYPE": type
            },
        };

        return this.docClient.get(params).promise()
            .then((data: DynamoDB.GetItemOutput) => {
                return data.Item
            })
    }

    /**
     * Permet de setter une valeur sur une clé
     *
     * @param userId l'id du l'utilisateur
     * @param type le 2nd id pour identifier le tuple
     * @param key la clé
     * @param values la valeur à save string | number | array | object
     * @returns la valeur enregistré
     */
    put(userId: string, type: string, key: string,  values: any): Promise<any>
    {
        var query = "SET #Key = :value";
        var params = {
            TableName: this.TABLE_NAME,
            Key: {
                "USER_ID": userId,
                "TYPE": type
            },
            ExpressionAttributeNames : {
                "#Key" : key
            },
            ExpressionAttributeValues: {
                ':value' : values,
            },
            UpdateExpression: query,
            ReturnValues: 'UPDATED_NEW'
        };


        return this.docClient.update(params).promise()
            .then((data: DynamoDB.PutItemOutput) => {
                return data.Attributes
        });
    }

    /**
     *
     * @param userId
     * @param type
     * @param key
     */
    delete(userId: string, type: string, key: string): Promise<any>
    {
        var query = `REMOVE #Key`;
        var params = {
            TableName: this.TABLE_NAME,
            Key: {
                "USER_ID": userId,
                "TYPE": type
            },
            ExpressionAttributeNames: {
                '#Key' : key,
            },
            UpdateExpression : query,
            ReturnValues: 'UPDATED_OLD'
        };

        return this.docClient.update(params).promise()
            .then((data: DynamoDB.PutItemOutput) => {
                return data.Attributes
        });
    }
}
