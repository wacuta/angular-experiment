import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { map, Observable, of } from "rxjs";
import { AwsService } from "../service/aws-service";

@Injectable()
export class PreferenceResolver  {

    constructor(private service: AwsService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {


    // this.service.createTable();
    // this.service.deleteTable();
    this.service.describeTable();

    // this.service.getPreference('123456789');

    return of([])
  }
}
