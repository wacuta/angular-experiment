import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AwsDynamodbComponent } from './aws-dynamodb.component';

describe('AwsDynamodbComponent', () => {
  let component: AwsDynamodbComponent;
  let fixture: ComponentFixture<AwsDynamodbComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AwsDynamodbComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AwsDynamodbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
