import { Component } from '@angular/core';
import { FabMultiFunctionTypeEnum } from '../components/fab-multi-function/fab-multi-function-type.enum';

@Component({
  selector: 'app-sandbox',
  templateUrl: './sandbox.component.html',
  styleUrls: ['./sandbox.component.scss']
})
export class SandboxComponent {

  readonly hoverMode = FabMultiFunctionTypeEnum.HOVER;
  readonly clickMode = FabMultiFunctionTypeEnum.CLICK;

  mode = FabMultiFunctionTypeEnum.HOVER;


  show(text: string): void {
    console.log(text);
  }

  action(): void {
    console.log('main action')
  }

}
