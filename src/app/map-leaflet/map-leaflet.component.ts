import { Component } from '@angular/core';
import { FeatureGroup, LatLng, circle, featureGroup, latLng, polygon, tileLayer } from 'leaflet';

var L = window.L;

@Component({
  selector: 'app-map-leaflet',
  templateUrl: './map-leaflet.component.html',
  styleUrls: ['./map-leaflet.component.scss']
})
export class MapLeafletComponent {

  enableEdit = true;

  /* Option premier affichage */
  options = {
    layers: [
      tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 18, attribution: '...' }),
    ],
    zoom: 12,
    center: latLng(43.6, 7)
  };

  /* Option lié au control pour changer le fond */
  optionsLayersControl = {
    baseLayers: {
      'Open Street Map': tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 18, attribution: '...' }),
      'Google Map': tileLayer('http://{s}.google.com/vt/lyrs=m&x={x}&y={y}&z={z}',{ maxZoom: 18, attribution: '...', subdomains:['mt0','mt1','mt2','mt3']})
    },
    overlays: {
      'Big Circle': circle([ 43.6, 7 ], { radius: 5000 }),
      'Big Square': polygon([[ 43.6, 7 ], [ 43.6, 7.01 ], [ 43.61, 7.01 ], [ 43.61, 7 ]])
    }
  };

  drawnItems: FeatureGroup = featureGroup();

  drawOptions = {
    draw: {
      circle: false as false,
      circlemarker: false as false,
      polygon: false as false,
      marker: {
        icon: L.icon({
          iconSize: [ 27, 37.5 ],
          iconAnchor: [ 13, 41 ],
          iconUrl: 'assets/map-leaflet/markers/google-pin.png',
        })
      },
      rectangle: false as false
      // rectangle: {
      //   shapeOptions: {
      //     color: '#85bb65'
      //   }
      // },

    },
    edit: {
      featureGroup: this.drawnItems
    },

  };

  polylines: L.Polyline[] = []

  ngOnInit(): void {
    const p1 = new L.Polyline([ { "lat": 43.60488338255023, "lng": 6.938037872314454 }, { "lat": 43.59021392903945, "lng": 6.943531036376954 } ]);
    const p2 = new L.Polyline([ { "lat": 43.59021392903945, "lng": 6.943531036376954 }, { "lat": 43.59170590122425, "lng": 6.975116729736329 } ]);
    const p3 = new L.Polyline([ { "lat": 43.59170590122425, "lng": 6.975116729736329 }, { "lat": 43.60115419950797, "lng": 7.009449005126954 } ]);
    const p4 = new L.Polyline([ { "lat": 43.60115419950797, "lng": 7.009449005126954 }, { "lat": 43.618057977924266, "lng": 6.999492645263673 } ]);
    this.drawnItems.addLayer(p1);
    this.drawnItems.addLayer(p2);
    this.drawnItems.addLayer(p3);
    this.drawnItems.addLayer(p4);
    this.polylines.push(p1)
    this.polylines.push(p2)
    this.polylines.push(p3)
    this.polylines.push(p4)
  }

  public onDrawCreated(e: any) {
    // this.drawnItems.addLayer((e as DrawEvents.Created).layer);
    const layer = e.layer;
    if (layer instanceof L.Polyline) {
      const newLigne: LatLng[] = layer.getLatLngs() as LatLng[];
      for (let i = 0; i < newLigne.length - 1; i++) {
        if (i + 1 < newLigne.length) {
          const onePoly = new L.Polyline([newLigne[i], newLigne[i + 1]]);
          this.drawnItems.addLayer(onePoly);
          this.polylines.push(onePoly)
        }
      }
    }
    if (layer instanceof L.Marker) {
      this.drawnItems.addLayer(layer);
    }
  }

  public leafletDrawStart(e: any) {
    console.log("DrawStart", e.layerType)
  }

  public leafletDrawStop(e: any) {
    console.log("DrawStop", e)
  }

  public leafletDrawDeleted(e: any) {
    console.log("DrawDeleted", e)
    const layers = e.layers.getLayers();
    console.log(layers)
    layers.forEach((element: any) => {
      if (element instanceof L.Polyline) {
        console.log(element)
      }
    });
  }


}
