
# Leaflet for Angular

Installation

```
npm i leaflet [@asymmetrik/ngx-leaflet](https://www.npmjs.com/package/@asymmetrik/ngx-leaflet)
npm i leaflet-draw @asymmetrik/ngx-leaflet-draw
npm i --save-dev @types/leaflet @types/leaflet-draw
```

Ajout du style dans `angular.json`

```json
  "styles": [
    "src/styles.scss",
    "./node_modules/leaflet/dist/leaflet.css",
    "./node_modules/leaflet-draw/dist/leaflet.draw.css"
  ],
```

Déclaration du module à l'application

```ts
import { LeafletModule } from '@asymmetrik/ngx-leaflet';

...
imports: [
	...
	LeafletModule,
  LeafletDrawModule
]
```

## Remarque

L'édition demanderais beaucoup de balisage pour que l'utilisateur ne fasse pas n'importe quoi. De plus, il faut tout convertir avec l'API google.
On va voir si c'est pas plus simple de faire directement avec l'API Google MAP
