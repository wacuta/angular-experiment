
import { Component, ViewChild } from '@angular/core';
import PolylineOptions = google.maps.PolylineOptions;
import InfoWindowOptions =google.maps.InfoWindowOptions;
import { GoogleMap, MapInfoWindow, MapMarker } from '@angular/google-maps';

@Component({
  selector: 'app-map-google',
  templateUrl: './map-google.component.html',
  styleUrls: ['./map-google.component.scss']
})
export class MapGoogleComponent {

  withFrame = 900;
  heightFrame = 500;

  heightMarker = 50;

  @ViewChild(GoogleMap, { static: false }) map!: GoogleMap

  @ViewChild(MapInfoWindow) infoWindow!: MapInfoWindow;
  infoWindowOption: InfoWindowOptions = {
    maxWidth: 700,
    disableAutoPan: true
  }

  actionSelected: string = '';

  drawingManager: google.maps.drawing.DrawingManager = new google.maps.drawing.DrawingManager();
  drawingManagerOptions: google.maps.drawing.DrawingManagerOptions = {
    drawingControl: true,
    // drawingMode: google.maps.drawing.OverlayType.POLYLINE,
    drawingControlOptions: {
      position: google.maps.ControlPosition.TOP_CENTER,
      drawingModes: [
        google.maps.drawing.OverlayType.POLYLINE,
        google.maps.drawing.OverlayType.CIRCLE,
        google.maps.drawing.OverlayType.MARKER,
      ],
    },
    polylineOptions: {
      strokeColor: "#35AFE9",
      editable: true,
      draggable: true,
      geodesic: true
    }
  }

  polyLinesToDisplay: PolylineOptions[] = [];

  polylines: google.maps.Polyline[] = []

  polylinesHTML: google.maps.Polyline[] = [];

  mapCenter = {lat: 43.6, lng: 7}

  options: google.maps.MapOptions = {
    center: this.mapCenter,
    zoom: 16
  }

  drawMode: boolean = true;
  toggleDrawControl() {
    this.drawMode = !this.drawMode;
    this.drawingManager.setOptions({
      drawingControl: this.drawMode,
      polylineOptions: {
        editable: this.drawMode,
       }
    })
  }

  onCompletePolyline(polyline: google.maps.Polyline) {
    this.polylines.push(polyline);
    this.polylinesHTML = [...this.polylines];
    var polylinePath = polyline.getPath();
    console.log("polyline : " + polylinePath.getArray());
  }

  ngAfterViewInit() {
    this.drawingManager = new google.maps.drawing.DrawingManager(this.drawingManagerOptions);
    this.drawingManager.setMap(this.map.googleMap!);

    google.maps.event.addListener(this.drawingManager, 'polylinecomplete', (polyline: google.maps.Polyline) => this.onCompletePolyline(polyline));

    this.map.googleMap?.addListener("click", (e: google.maps.MapMouseEvent) => {
      switch (this.actionSelected) {
        case 'marker':
          this.addMarker(e);
          break;
        default:
          console.log("click");
      }
    })

  }

  addMarker(e: google.maps.MapMouseEvent): void {
    let marker = new google.maps.Marker({
      position: e.latLng,
      map: this.map.googleMap,
    });
    this.map.panTo(e.latLng!);
  }

  openInfoWindow(mapMarker: MapMarker): void {

    let scale = Math.pow(2, this.map.getZoom()!);
    let nw = new google.maps.LatLng(
        this.map.getBounds()?.getNorthEast().lat()!,
        this.map.getBounds()?.getSouthWest().lng()
    );
    let worldCoordinateNW = this.map.getProjection()?.fromLatLngToPoint(nw);
    let worldCoordinate = this.map.getProjection()?.fromLatLngToPoint(mapMarker.getPosition()!);
    let pixelOffset = new google.maps.Point(
      Math.floor((worldCoordinate!.x - worldCoordinateNW!.x) * scale),
      Math.floor((worldCoordinate!.y - worldCoordinateNW!.y) * scale)
    );

    const widthInfoWindow = 700 / 2;
    const heightInfoWindow = 75;
    const heightmarker = 50;
    let x = 0;
    let y = 0;

    if(pixelOffset.x < widthInfoWindow) x = widthInfoWindow - pixelOffset.x;
    if(pixelOffset.y < (heightmarker + heightInfoWindow)) y = heightmarker + heightInfoWindow;

    if(this.withFrame - pixelOffset.x < widthInfoWindow) x = -widthInfoWindow + (this.withFrame - pixelOffset.x);
    if(this.heightFrame - pixelOffset.y < heightInfoWindow) y = 0;

    this.infoWindowOption.pixelOffset = new google.maps.Size(x, y)
    this.infoWindow.options = this.infoWindowOption;
    this.infoWindow.open(mapMarker);
  }

}
