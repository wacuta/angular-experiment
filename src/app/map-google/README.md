
# Google-Map for Angular

Installation

```bash
npm i [@angular/google-maps](https://github.com/angular/components/tree/main/src/google-maps)
```

```html
<!-- index.html -->
<!doctype html>
<head>
  ...
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY"></script>
</head>
```

```ts
import { GoogleMapsModule } from '@angular/google-maps';

...
imports: [
	...
	GoogleMapsModule,
]
```
