import PolylineOptions = google.maps.PolylineOptions;

export interface LineShapeInterface {
  encoded_positions: string;
  start_bus_stop: any;
  end_bus_stop: any;
  poly_line_options: PolylineOptions;
}
